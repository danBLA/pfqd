# -*- coding: utf-8 -*-
#   Copyright 2020-2021 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import logging
import subprocess
import json


class QReader(object):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
    

    def _read_queue(self):
        p = subprocess.Popen(['/usr/sbin/postqueue', '-j'],stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        lines = p.stdout.read().splitlines()
        self.logger.debug('read %s lines' % len(lines))
        return lines
    
    
    def _parse_json(self, lines):
        queues = {}
        for line in lines:
            try:
                m = json.loads(line)
                try:
                    queues[m['queue_name']].append(m)
                except KeyError:
                    queues[m['queue_name']] = [m]
            except Exception as e:
                self.logger.error(f'Failed to parse queue line "{line}": {str(e)}')
        return queues
    
    
    
    def read(self):
        lines = self._read_queue()
        queues = self._parse_json(lines)
        return queues
    

