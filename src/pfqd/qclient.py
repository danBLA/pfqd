# -*- coding: UTF-8 -*-
#   Copyright 2020-2021 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import re
import logging
from .qstore import QStore


class PFQDClient(object):
    def __init__(self, args):
        self.args = args
        self.logger = logging.getLogger(self.__class__.__name__)
    
    
    def _parse_queuename(self, value):
        if ':' in value:
            queue, hostrgx = value.split(':', 1)
            rgx = re.compile(hostrgx.encode(), re.I)
        else:
            queue = value
            rgx = None
        return queue, rgx
        
    
    def read(self):
        qstore = QStore(self.args)
        if self.args.summary:
            queue, rgx = self._parse_queuename(self.args.summary)
            queue_data = qstore.get_summary(queue, rgx)
            for line in queue_data:
                print('\t'.join(line))
        elif self.args.full:
            queue, rgx = self._parse_queuename(self.args.full)
            queue_data = qstore.get_full(queue, rgx)
            for line in queue_data:
                print(line)


