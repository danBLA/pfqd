# -*- coding: utf-8 -*-
#   Copyright 2020-2021 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import argparse
import configparser
import os
import sys
import socket
import logging
from .qserver import PFQD
from .qclient import PFQDClient
try:
    import redis
    REDIS_AVAILABLE = True
except ImportError:
    REDIS_AVAILABLE = False

LOG_FORMAT_STDOUT='%(asctime)s %(name)s %(levelname)s %(message)s'


def get_hostname():
    hostname = os.environ.get('SWARM_NODE')
    if not hostname:
        hostname = socket.gethostname()
    return hostname



def load_config():
    conf_parser = argparse.ArgumentParser(add_help=False)
    conf_parser.add_argument('--config', help='path to configuration file', default='/etc/pfqd/pfqd.ini', metavar="FILE")
    args, remaining_argv = conf_parser.parse_known_args()
    
    defaults = {'redisconn': os.environ.get('REDIS_CONN')}
    if args.config and os.path.exists(args.config):
        config = configparser.ConfigParser()
        config.read([args.config])
        defaults.update(dict(config.items("pfqd")))
    elif args.config and not os.path.exists(args.config):
        print(f'ERROR: config file {args.config} does not exist')
        sys.exit(1)
    
    parser = argparse.ArgumentParser(parents=[conf_parser])
    parser.set_defaults(**defaults)
    parser.add_argument('--hostname', help='set host name', default=get_hostname())
    parser.add_argument('--redisconn', help='redis connection URI ( redis://host:port/db )')
    parser.add_argument('--refresh', help='queue refresh interval', type=int, default=15)
    parser.add_argument('--ttl', help='default redis ttl', type=int, default=60)
    parser.add_argument('--verbose', help='enable debug output', action='store_true')
    parser.add_argument('--summary', help='get summary queue and quit. format: queue_name[:hostrgx]', default='')
    parser.add_argument('--full', help='get full queue and quit. format: queue_name[:hostrgx]', default='')
    args = parser.parse_args(remaining_argv)
    
    if not args.redisconn:
        print('ERROR: no redis connection set')
        sys.exit(1)
    if not args.redisconn.startswith('redis://'):
        print(f'ERROR: not a valid redis:// url: {args.redisconn}')
        sys.exit(1)
    return args
    
    

def main():
    args = load_config()
    if args.verbose:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO
    logging.basicConfig(format=LOG_FORMAT_STDOUT, level=loglevel)
    
    if not REDIS_AVAILABLE:
        print('ERROR: python redis module not available')
        sys.exit(1)
    
    if args.summary or args.full:
        c = PFQDClient(args)
        c.read()
    else:
        p = PFQD(args)
        p.start()



if __name__ == "__main__":
    main()
    
    