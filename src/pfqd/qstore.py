# -*- coding: utf-8 -*-
#   Copyright 2020-2021 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import logging
import time
import base64
import pickle
import redis



class QStore(object):
    QUEUE_ACTIVE = 'active'
    QUEUE_DEFERRED = 'deferred'
    QUEUE_HOLD = 'hold'
    QUEUE_INCOMING = 'incoming'
    
    _queues = [QUEUE_ACTIVE, QUEUE_DEFERRED, QUEUE_HOLD, QUEUE_INCOMING]
    
    
    
    def __init__(self, args):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.ttl = args.ttl
        self.hostname = args.hostname
        self.redisconn = args.redisconn
        self.redis_pool = None
        
        
        
    def _get_redis(self):
        if self.redis_pool is None:
            self.redis_pool = redis.ConnectionPool.from_url(self.redisconn)
        redisconn = redis.StrictRedis(connection_pool=self.redis_pool, socket_keepalive=True, socket_timeout=60)
        return redisconn
    
    
    
    def _b64pickle(self, value):
        return base64.b64encode(pickle.dumps(value, pickle.HIGHEST_PROTOCOL))
    
    
    
    def _b64unpickle(self, value):
        return pickle.loads(base64.b64decode(value))
    
    
    
    def _check_queue_name(self, queue_name):
        ok = True
        if queue_name not in self._queues:
            self.logger.info(f'queue_name {queue_name} not one of {",".join(self._queues)}')
            ok = False
        return ok
    
    
    
    def _check_hostrgx(self, hostrgx):
        if hostrgx is not None:
            if hostrgx.__class__.__name__ not in ['Pattern', 'SRE_Pattern']:
                # not the pythonic way, but we avoid import of _sre
                # python 3.6: _sre.SRE_Pattern
                # python >= 3.7: re.Pattern
                raise TypeError(f'hostrx is not of type re.Pattern type is {hostrgx.__class__.__name__}')
            if not isinstance(hostrgx.pattern, bytes):
                raise TypeError('hostrgx pattern must be of type bytes')
    
    
    
    def set_summary(self, queue_name, summary_table):
        if not self._check_queue_name(queue_name):
            return
        if summary_table:
            redisconn = self._get_redis()
            ts = time.time()
            redisconn.hset('pfqdsum_%s' % queue_name, self.hostname, self._b64pickle((ts, summary_table)))
            self.logger.debug('summary: stored %s entries' % len(summary_table))
        else:
            self.logger.debug('summary: nothing to store')
    
    
    
    def get_summary(self, queue_name, hostrgx=None):
        """
        get queue summary for queue queue_name and optionally hosts defined by hostrgx
        :param queue_name: name of queue (active, deferred, hold)
        :param hostrgx: regular expression defining hosts for which to get queue
        :return: list of host queue values
        """
        summary_table = []
        if not self._check_queue_name(queue_name):
            return summary_table
        self._check_hostrgx(hostrgx)
        
        redisconn = self._get_redis()
        try:
            data = redisconn.hgetall('pfqdsum_%s' % queue_name)
        except redis.exceptions.ResponseError as e:
            self.logger.error(f'failed to get summary due to {str(e)}')
            data = None
        if data is None:
            self.logger.error('summary: no data retrieved')
            return summary_table
        exp = time.time()-self.ttl
        for host in data:
            if hostrgx is None or hostrgx.match(host):
                ts, host_table = self._b64unpickle(data[host])
                if ts > exp:
                    summary_table.extend(host_table)
                else:
                    host = host.decode()
                    self.logger.info('summary: ignoring stale data from %s' % host)
        return summary_table
    
    
    
    def set_full(self, queue_name, queue_data):
        if not self._check_queue_name(queue_name):
            return
        if queue_data:
            redisconn = self._get_redis()
            ts = time.time()
            redisconn.hset('pfqd_%s' % queue_name, self.hostname, self._b64pickle((ts, queue_data)))
            self.logger.debug('full: stored %s entries' % len(queue_data))
        else:
            self.logger.debug('full: nothing to store')
    
    
    
    def get_full(self, queue_name, hostrgx=None):
        """
        get full queue for queue queue_name and optionally hosts defined by hostrgx
        :param queue_name: name of queue (active, deferred, hold)
        :param hostrgx: regular expression defining hosts for which to get queue
        :return: list of host queue values
        """
        
        queue_data = []
        if not self._check_queue_name(queue_name):
            return queue_data
        self._check_hostrgx(hostrgx)
        
        redisconn = self._get_redis()
        try:
            data = redisconn.hgetall('pfqd_%s' % queue_name)
        except redis.exceptions.ResponseError as e:
            self.logger.error(f'failed to get data due to {str(e)}')
            data = None
        if data is None:
            self.logger.error('full: no data retrieved')
            return queue_data
        exp = time.time()-self.ttl
        for host in data:
            if hostrgx is None or hostrgx.match(host):
                ts, host_table = self._b64unpickle(data[host])
                if ts > exp:
                    queue_data.extend(host_table)
                else:
                    self.logger.info('full: ignoring stale data from %s' % host)
        return queue_data
    
    
    